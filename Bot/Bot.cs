﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InputFiles;
using Telegram.Bot.Types.ReplyMarkups;

namespace Bot
{
    public class Bot
    {
        public Bot()
        {
            //TODO: read these from database
            //GroupIds.Add(-374925338, "Tunkkaus");
            GroupIds.Add(-1001235503801, "Hesenyymit 2.0");
        }

        public TelegramBotClient TelegramClient { get; set; }

        public User TelegramSelf { get; set; }

        public async Task StartBot()
        {
            TelegramClient = new TelegramBotClient(ConfigurationManager.AppSettings["BotToken"]);
            TelegramSelf = await TelegramClient.GetMeAsync();
            Console.WriteLine($"Logged in as @{TelegramSelf.Username}");

            TelegramClient.OnUpdate += Bot_OnUpdate;
            TelegramClient.StartReceiving();
        }

        public void StopBot()
        {
            TelegramClient.StopReceiving();
        }

        private readonly Dictionary<long, string> GroupIds = new Dictionary<long, string>();

        private static readonly List<BotUser> Users = new List<BotUser>();
        private static readonly List<Message> OriginalMessages = new List<Message>();

        private async void Bot_OnUpdate(object sender, UpdateEventArgs e)
        {
            try
            {
                if (e.Update.Type == UpdateType.Message)
                {
                    var message = e.Update.Message;

                    if (message.Chat.Type == ChatType.Private)
                    {
                        var entity = message.Entities?.FirstOrDefault();
                        if (entity != null && entity.Type == MessageEntityType.BotCommand && message.EntityValues.First() == "/start")
                        {
                            var text = $"Tervetuloa käyttämään @{TelegramSelf.Username}!";
                            text += Environment.NewLine;
                            text += Environment.NewLine;
                            text += $"Ominaisuudet:{Environment.NewLine}";
                            text += $"- Lähetä botille viesti niin se välittää sen anonyymisti valittuun ryhmään{Environment.NewLine}";
                            text += $"- Kirjoita viestiin (joko botille lähetettävään tai ryhmään suoraan) joko #3sec tai #5min niin botti poistaa kyseisen viestin joko 3 sekuntin tai 5 minuutin päästä (ajan voi valita vapaasti){Environment.NewLine}";
                            text += Environment.NewLine;
                            text += $"Uusia ominaisuuksia saa ehdottaa sekä bugeja ilmoittaa lähettämällä viestiä @Joose{Environment.NewLine}";

                            await TelegramClient.SendTextMessageAsync(message.From.Id, text);
                            return;
                        }

                        var user = Users.SingleOrDefault(u => u.UserId == message.From.Id);
                        if (user == null)
                        {
                            user = new BotUser { UserId = message.From.Id, LastMessage = DateTime.UtcNow };
                            Users.Add(user);
                        }
                        else if (user.LastMessage.AddSeconds(10) > DateTime.UtcNow)
                        {
                            await TelegramClient.SendTextMessageAsync(message.From.Id, "Error 420: Enhance your calm.", replyToMessageId: message.MessageId);
                            return;
                        }
                        else
                            user.LastMessage = DateTime.UtcNow;

                        var buttons = new List<InlineKeyboardButton>();
                        foreach (var groupId in GroupIds)
                        {
                            var member = await TelegramClient.GetChatMemberAsync(groupId.Key, message.From.Id);
                            if (member.Status >= ChatMemberStatus.Creator && member.Status <= ChatMemberStatus.Member)
                            {
                                var group = await TelegramClient.GetChatAsync(groupId.Key);
                                var callbackData = new CallbackData
                                {
                                    OriginalMessageId = message.MessageId,
                                    SelectedGroupId = group.Id,
                                    Action = CallbackDataAction.SelectDelay
                                };
                                buttons.Add(new InlineKeyboardButton { Text = group.Title, CallbackData = JsonConvert.SerializeObject(callbackData) });
                            }
                        }

                        if (buttons.Any())
                        {
                            OriginalMessages.Add(message);
                            var keyboard = new InlineKeyboardMarkup(buttons);
                            await TelegramClient.SendTextMessageAsync(message.From.Id, "Valitse ryhmä", replyToMessageId: message.MessageId, replyMarkup: keyboard);
                        }
                        else
                            await TelegramClient.SendTextMessageAsync(message.From.Id, "Tuntematon ryhmä.");
                    }
                    else if ((int)message.Chat.Type >= 1)
                    {
                        var groupId = GroupIds.Where(gi => gi.Key == message.Chat.Id).Select(gi => gi.Value).SingleOrDefault();
                        if (groupId != null)
                        {
                            /*
                            var command = message.Entities.FirstOrDefault(ent => ent.Type == MessageEntityType.BotCommand);
                            if (command != null)
                            {
                                var index = Array.FindIndex(message.Entities, ent => ent == command);
                                var commandValue = message.EntityValues.ElementAt(index);
                                if (commandValue == "/restrict" && message.ReplyToMessage != null)
                                {
                                    
                                }
                            }
                            */

                            // Delete message after given delay, format is #5min or #5sec
                            var deleteDelay = GetDeleteDelay(message);
                            if (deleteDelay.Ticks > 0)
                            {
                                await Task.Delay(deleteDelay).ContinueWith(t =>
                                {
                                    return TelegramClient.DeleteMessageAsync(message.Chat.Id, message.MessageId);
                                });
                            }
                        }
                    }
                }
                else if (e.Update.Type == UpdateType.CallbackQuery)
                {
                    var callback = e.Update.CallbackQuery;
                    var callbackData = JsonConvert.DeserializeObject<CallbackData>(callback.Data);

                    if (callbackData.Action == CallbackDataAction.SelectDelay)
                    {
                        await TelegramClient.DeleteMessageAsync(callback.From.Id, callback.Message.MessageId);

                        var buttons = new List<InlineKeyboardButton>();
                        callbackData.Action = CallbackDataAction.SendMessage;

                        callbackData.Delay = CallbackDataDelay.Immediate;
                        buttons.Add(new InlineKeyboardButton { Text = "Heti", CallbackData = JsonConvert.SerializeObject(callbackData) });

                        callbackData.Delay = CallbackDataDelay.From30sto1min;
                        buttons.Add(new InlineKeyboardButton { Text = "30-60 sek", CallbackData = JsonConvert.SerializeObject(callbackData) });

                        callbackData.Delay = CallbackDataDelay.From1to5min;
                        buttons.Add(new InlineKeyboardButton { Text = "1-5 min", CallbackData = JsonConvert.SerializeObject(callbackData) });

                        callbackData.Delay = CallbackDataDelay.From5to10min;
                        buttons.Add(new InlineKeyboardButton { Text = "5-10 min", CallbackData = JsonConvert.SerializeObject(callbackData) });

                        var keyboard = new InlineKeyboardMarkup(buttons);
                        await TelegramClient.SendTextMessageAsync(callback.From.Id, "Valitse viestin lähetysaika", replyToMessageId: callbackData.OriginalMessageId, replyMarkup: keyboard);
                    }
                    else if (callbackData.Action == CallbackDataAction.SendMessage)
                    {
                        await TelegramClient.DeleteMessageAsync(callback.From.Id, callback.Message.MessageId);

                        var originalMessage = OriginalMessages.Where(om => om.MessageId == callbackData.OriginalMessageId).SingleOrDefault();
                        if (originalMessage != null)
                        {
                            var groupId = GroupIds.Where(gi => gi.Key == callbackData.SelectedGroupId).Select(gi => gi.Key).Cast<long?>().SingleOrDefault();
                            if (groupId != null)
                            {
                                var delay = GetSendDelay(callbackData.Delay ?? CallbackDataDelay.Immediate);
                                await Task.Delay(delay).ContinueWith(t =>
                                {
                                    return ForwardMessage(originalMessage, groupId.Value);
                                });
                            }
                            else
                                await TelegramClient.SendTextMessageAsync(callback.From.Id, "Tuntematon ryhmä.");

                            OriginalMessages.Remove(originalMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error! Exception: {ex.Message}");
                Console.WriteLine(ex.StackTrace);
            }
        }

        private async Task<Message> ForwardMessage(Message message, long targetGroupId)
        {
            Message sentMessage = null;

            switch (message.Type)
            {
                case MessageType.Text:
                    sentMessage = await TelegramClient.SendTextMessageAsync(targetGroupId, message.Text); //TODO: implement parsemode to properly forward styled text
                    break;
                case MessageType.Poll:
                    sentMessage = await TelegramClient.SendPollAsync(targetGroupId, message.Poll.Question, message.Poll.Options.Select(o => o.Text));
                    break;
                case MessageType.Photo:
                    var photo = message.Photo.OrderByDescending(p => p.FileSize).FirstOrDefault(); //TODO: how to select correct picture?
                    if (photo != null)
                        sentMessage = await TelegramClient.SendPhotoAsync(targetGroupId, new InputOnlineFile(photo.FileId), message.Caption);
                    break;
                case MessageType.Sticker:
                    sentMessage = await TelegramClient.SendStickerAsync(targetGroupId, new InputOnlineFile(message.Sticker.FileId));
                    break;
                case MessageType.Document:
                    sentMessage = await TelegramClient.SendDocumentAsync(targetGroupId, new InputOnlineFile(message.Document.FileId), message.Caption, thumb: (message.Document.Thumb != null ? new InputMedia(message.Document.Thumb.FileId) : null));
                    break;
                case MessageType.Video:
                    sentMessage = await TelegramClient.SendVideoAsync(targetGroupId, new InputOnlineFile(message.Video.FileId), thumb: (message.Video.Thumb != null ? new InputMedia(message.Video.Thumb.FileId) : null));
                    break;
                case MessageType.VideoNote:
                    sentMessage = await TelegramClient.SendVideoNoteAsync(targetGroupId, new InputOnlineFile(message.VideoNote.FileId), thumb: (message.VideoNote.Thumb != null ? new InputMedia(message.VideoNote.Thumb.FileId) : null));
                    break;
                case MessageType.Voice:
                    sentMessage = await TelegramClient.SendVoiceAsync(targetGroupId, new InputOnlineFile(message.Voice.FileId));
                    break;
                case MessageType.Audio:
                    sentMessage = await TelegramClient.SendAudioAsync(targetGroupId, new InputOnlineFile(message.Audio.FileId));
                    break;
                default:
                    await TelegramClient.SendTextMessageAsync(message.From.Id, $"Viestityyppiä <strong>MessageType.{message.Type.ToString()}</strong> ei vielä tueta.", ParseMode.Html);
                    return null;
            }

            await TelegramClient.SendTextMessageAsync(message.From.Id, "Viesti lähetetty.");

            // Delete forwarded message after given delay, format is #5min or #5sec
            var deleteDelay = GetDeleteDelay(message);
            if (deleteDelay.Ticks > 0)
            {
                await Task.Delay(deleteDelay).ContinueWith(t =>
                {
                    return TelegramClient.DeleteMessageAsync(sentMessage.Chat.Id, sentMessage.MessageId);
                });
            }

            return sentMessage;
        }

        private TimeSpan GetDeleteDelay(Message message)
        {
            // Look for all hashtag message entities and select the value from the first one that matches regex
            var tags = new List<string>();
            if (message.Entities != null)
            {
                foreach (var tag in message.Entities.Where(e => e.Type == MessageEntityType.Hashtag))
                {
                    var index = Array.FindIndex(message.Entities, e => e == tag);
                    tags.Add(message.EntityValues.ElementAt(index));
                }
            }
            else if (message.CaptionEntities != null)
            {
                foreach (var tag in message.CaptionEntities.Where(ce => ce.Type == MessageEntityType.Hashtag))
                {
                    var index = Array.FindIndex(message.CaptionEntities, ce => ce == tag);
                    tags.Add(message.CaptionEntityValues.ElementAt(index));
                }
            }

            var r = new Regex(@"^#(?:(\d*)m(?:in)?)?(?:(\d*)s(?:ec|ek)?)?$");
            foreach (var tag in tags)
            {
                var m = r.Match(tag);
                if (m.Success)
                {
                    int minutes = (m.Groups[1].Success ? int.Parse(m.Groups[1].Value) : 0); // first group is minutes
                    int seconds = (m.Groups[2].Success ? int.Parse(m.Groups[2].Value) : 0); // second group is seconds
                    return new TimeSpan(0, minutes, seconds); // Just select the first tag that matches and return
                }
            }

            return new TimeSpan(0);
        }

        private int GetSendDelay(CallbackDataDelay delay)
        {
            var r = new Random();
            switch (delay)
            {
                case CallbackDataDelay.From30sto1min:
                    return r.Next(30000, 60000);
                case CallbackDataDelay.From1to5min:
                    return r.Next(60000, 5 * 60000);
                case CallbackDataDelay.From5to10min:
                    return r.Next(5 * 60000, 10 * 60000);
                default:
                    return 0;
            }
        }
    }

    /// <summary>
    /// Callback data has a limit of 64 bits. That's why property names are shortened to one letter
    /// </summary>
    public class CallbackData
    {
        [JsonProperty("o")]
        public int OriginalMessageId { get; set; }

        [JsonProperty("s")]
        public long SelectedGroupId { get; set; }

        [JsonProperty("a")]
        public CallbackDataAction Action { get; set; }

        [JsonProperty("d", NullValueHandling = NullValueHandling.Ignore)]
        public CallbackDataDelay? Delay { get; set; }

    }

    public enum CallbackDataAction
    {
        SendMessage = 0,
        SelectDelay = 1
    }

    public enum CallbackDataDelay
    {
        Immediate = 0,
        From30sto1min = 1,
        From1to5min = 2,
        From5to10min = 3
    }

    public class BotUser
    {
        public BotUser()
        {
            MessageIds = new List<long>();
        }

        public DateTime? Restricted { get; set; }
        public long UserId { get; set; }
        public DateTime LastMessage { get; set; }
        public List<long> MessageIds { get; set; }
    }
}
