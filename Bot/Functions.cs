﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Telegram.Bot;
using Telegram.Bot.Args;

namespace Bot
{
    public class Functions
    {
        // This function will get triggered/executed when a new message is written 
        // on an Azure Queue called queue.
        public static void ProcessQueueMessage([QueueTrigger("queue")] string message, TextWriter log)
        {
            //TODO: use this for something useful, like restarting bot or something
            log.WriteLine(message);
        }

        [NoAutomaticTrigger]
        public static async Task StartBot(TextWriter log)
        {
            var bot = new Bot();
            await bot.StartBot();
        }
    }
}
